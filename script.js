
const getCube = 2 ** 3
console.log(getCube)

console.log(`The cube of 2 is ${getCube}`);


const address = ["B23", "San Pedro Laguna", "Philippines", "4023"];

const [houseNum, st, state, zip] = address

console.log(`I live at ${houseNum} ${st} ${state} ${zip}`)



const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}


const {name, species, weight, measurement} = animal

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)



let numbers = [1, 2, 3, 4, 5];


numbers.forEach(num => console.log(num))


let reduceNumber = numbers.reduce((x, y) => x + y)
console.log(reduceNumber)



class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed
	}
}



const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog)
console.log(myDog.name)
